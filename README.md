Gomando is a tool to collect and store commands.

Gomando Wiki
http://gomando.inss.ch/Main_Page

This is a first free beta version. It is solely for testing purpose and comes
naturally without any guarantees.
Gomando is a tool to store commands in a personal collection, that can be also
be shared in this wiki like e.g. a page for Git cheat collection.
Some first examples are in this Collection_List.
See Help for all parameters that can be used with Gomando.
Inspiration
As it happens many times, its inspiration came from a need. I am working
mainly with Linux systems, and you really have a lot of commands available.
But there are some commands, that I don't use often, and other ones have a lot
of parameters.
I don't want to look always. How does it work, what parameters are available
and how to set them correctly.
Another issue was that I sometimes needed very small command lines tools like
an email client or even a little fileserver.
So I started to pack altogether and wrote it using Golang. Golang is a
programming language and very useful for command line applications (though
not only).
Now it stores all commands you want (or even a script) in an xml. And you can
retrieve it any time. You can give it a name, or let Gomando define it.
You can give it tags and a description to always find it when you need to.
And because such a collection of data with a description can be useful for other
users, I created also this Wiki.
Here you can upload your data xml, download it from another device or share it
simply with all other users.
Have fun with this tool and share your collections!

Overview
This tool is mainly for storing and executing commands on command line. It
has currently two files: configuration.xml with some configuration options. And
xml with data named to the operationg system like "linux.xml" or
"windows.xml".
It is meant to keep commands that you need from time to time in a file. You
don't have to have all in your mind. Maybe it will become a pool for certain
work areas with an xml for each area. gomando -help will print all available
options.
It comes currently without packages and contains only single binaries for
Linux, Linux arm and Windows.
Usage
First steps with examples are:
Store a new entry like a command with gomando -new It will start a little wizard
with some questions like of what type that data will be. You can store values of
type:
 Value
 Command
 Web URL
 File
 Macro
If it is a value, it will only be printed to the screen.
if data is of type command, it will be executed.
If it is a web URL, it will open the browser with that URL.
If it is a file, it will spit out the file that has been stored to the filesystem.
If it is a macro, it will not do much since that feature is not yet implemented.
Store a new command with: gomando -command It stores the data without further
questions as a command with a calculated name. You can execute then the
command with that name. Example: gomando -command echo hello Output:
Input registered with name: "echo" and description "echo" Execute with:
gomando echo Output: Found command echo with description echo gomando is
executing command echo hello hello gomando is done
Data are stored in an xml, named after the operating system. Like linux.xml. It
will create also a configuration.xml which is used for some configuration data.


Execute gomando -help to get an overview of all parameter for Gomando.


THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIIMITED TO, THE IMPLIIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Downloads: https://gitlab.com/networkinss/Gomando/

02.09.2018
